//
// Created by gigi on 2/13/20.
//

#include "image.hh"
#include <iostream>
#include <fstream>
#include <string>

void Image::compute_image()
{
    std::ofstream file;
    file.open("render.ppm");

    file << "P3\n" << this->_width << " "<< this->_height << "\n255\n";
    for (int y = 0; y < this->_height; y++)
    {
        for (int x = 0; x < this->_width; x++)
        {
            auto pixel = this->_pixels[_width * y + x];
            file << pixel;
        }
        file << "\n";
    }
}

void Image::set_pixel(const int x, const int y, const Color &color)
{
    this->_pixels[_width * y + x] = Color(color);
}