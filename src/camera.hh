//
// Created by gigi on 2/14/20.
//

#ifndef RAYTRACER_CAMERA_HH
#define RAYTRACER_CAMERA_HH

#include "vector4.hh"

class Camera {
public:
    Camera()
    {}

    Camera(const Vector4 &center,const Vector4 &up,const Vector4 &forward, double fov)
        : _center(center)
        , _up(up)
        , _forward(forward)
        , _fov(fov)
    {
        _right = _forward.cross(_up).normalize();
    }

    Vector4 get_center() const
    {
        return _center;
    }
    Vector4 get_up() const
    {
        return _up;
    }
    Vector4 get_forward() const
    {
        return _forward;
    }
    Vector4 get_right() const
    {
        return _right;
    }
    float get_fov() const
    {
        return _fov;
    }

private:
    Vector4 _center;
    Vector4 _up;
    Vector4 _forward;
    Vector4 _right;

    float _fov;

};


#endif //RAYTRACER_CAMERA_HH
