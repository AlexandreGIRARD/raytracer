//
// Created by gigi on 2/13/20.
//

#include "vector4.hh"
#include <cmath>

Vector4 Vector4::operator*(const double lambda) const
{
    return Vector4(this->_x * lambda, this->_y * lambda, this->_z * lambda);
}

Vector4 Vector4::operator/(const double lambda) const
{
    return Vector4(double(this->_x) / lambda, double(this->_y) / lambda, double(this->_z) / lambda);
}

Vector4 Vector4::operator+(const Vector4 &vect) const
{
    return Vector4(this->_x + vect._x, this->_y + vect._y, this->_z + vect._z);
}

Vector4 Vector4::operator-(const Vector4 &vect) const
{
    return Vector4(this->_x - vect._x, this->_y - vect._y, this->_z - vect._z);
}

Vector4 Vector4::cross(const Vector4 &vect) const
{
    auto new_x = (this->_y * vect._z) - (this->_z * vect._y);
    auto new_y = (this->_z * vect._x) - (this->_x * vect._z);
    auto new_z = (this->_x * vect._y) - (this->_y * vect._x);
    return Vector4(new_x, new_y, new_z);
}

float Vector4::dot(const Vector4 &vect) const
{
    return this->_x * vect._x + this->_y * vect._y + this->_z * vect._z;
}

float Vector4::norm()
{
    return sqrt(this->dot(*this));
}

Vector4 Vector4::normalize()
{
    double lambda = sqrt(_x * _x + _y * _y + _z * _z);
    return *this / lambda;
}

float Vector4::distance(const Vector4 &rhs)
{
    return std::sqrt(pow(_x - rhs._x, 2) + pow(_y - rhs._y, 2) + pow(_z - rhs._z, 2));
}

std::ostream& operator<<(std::ostream &out, Vector4 &vect)
{
    return out << "x: " << vect._x << ", y: " << vect._y << ", z: " << vect._z << "\n";
}