//
// Created by gigi on 2/14/20.
//

#ifndef RAYTRACER_OBJECT_HH
#define RAYTRACER_OBJECT_HH

#include <optional>
#include <memory>

#include "vector4.hh"
#include "color.hh"
#include "material.hh"

class Object {
public:
    Object(const Vector4 &pos, const Material &material)
    : _position(pos)
    , _mtl(material)
    {}

    virtual std::optional<Vector4> intersection(const Vector4 &point,const Vector4 &vect) = 0;
    virtual Vector4 get_normal(const Vector4 &point) const = 0;
    virtual Material get_material() const
    {
        return _mtl;
    }

protected:
    Vector4 _position;
    Material _mtl;
};

#endif //RAYTRACER_OBJECT_HH
