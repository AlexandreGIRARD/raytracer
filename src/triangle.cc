//
// Created by gigi on 3/1/20.
//

#include <cmath>
#include "triangle.hh"

#define EPSILON  0.000001

std::optional<Vector4> Triangle::intersection(const Vector4 &point, const Vector4 &vect)
{
    Vector4 ab = _b - _position;
    Vector4 ac = _c - _position;
    Vector4 p_vec = vect.cross(ac);
    float det = ab.dot(p_vec);
    if (fabs(det) < EPSILON)
        return std::nullopt;

    Vector4 t_vec = point - _position;
    float u = t_vec.dot(p_vec) / det;
    if (u < 0 || u > 1)
        return std::nullopt;

    Vector4 q_vec = t_vec.cross(ab);
    float v = vect.dot(q_vec) / det;
    if (v < 0 || u + v > 1)
        return std::nullopt;

    float dist = ac.dot(q_vec) / det;
    return std::make_optional(point + vect * dist);
}

Vector4 Triangle::get_normal(const Vector4 &point) const
{
    return _normal;
}