//
// Created by gigi on 2/14/20.
//

#ifndef RAYTRACER_SPHERE_HH
#define RAYTRACER_SPHERE_HH

#include "vector4.hh"
#include "material.hh"
#include "light.hh"
#include "object.hh"
#include "json.hpp"

#include <optional>

class Sphere : public Object{
public:
    Sphere(const Vector4 &pos, const Material &material, double radius)
        : Object(pos, material)
        , _r(radius)
    {}

    Sphere(nlohmann::json j, Material &material)
        : Object(Vector4(j["position"]), material)
        , _r(j["radius"])
    {}

    std::optional<Vector4> intersection(const Vector4 &point, const Vector4 &vect) override ;
    Vector4 get_normal(const Vector4 &point) const override ;

    Vector4 get_position() const
    {
        return _position;
    }

private:
    double _r;
};


#endif //RAYTRACER_SPHERE_HH
