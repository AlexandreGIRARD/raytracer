//
// Created by gigi on 2/29/20.
//

#ifndef RAYTRACER_PLANE_HH
#define RAYTRACER_PLANE_HH

#include "json.hpp"
#include "object.hh"

class Plane : public Object {
public:
    Plane(const Vector4 &pos, const Material &material, const Vector4 &x, const Vector4 &y)
        : Object(pos, material)
        , _x(x)
        , _y(y)
    {
        _normal = _y.cross(_x).normalize();
    }

    Plane(nlohmann::json j, Material &material)
        : Object(Vector4(j["position"]), material)
        , _x(Vector4(j["x"]))
        , _y(Vector4(j["y"]))
    {
        _normal = _y.cross(_x).normalize();
    }
    std::optional<Vector4> intersection(const Vector4 &point,const Vector4 &vect) override ;
    Vector4 get_normal(const Vector4 &point) const override ;

private:
    Vector4 _x;
    Vector4 _y;
    Vector4 _normal;
};


#endif //RAYTRACER_PLANE_HH
