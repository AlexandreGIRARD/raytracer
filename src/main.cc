//
// Created by gigi on 2/14/20.
//
#include <iostream>

#include "image.hh"
#include "scene.hh"
#include "camera.hh"
#include "sphere.hh"
#include "plane.hh"
#include "light.hh"

int main(int argc, char *argv[])
{
    // Init camera and sphere
    /*auto camera = Camera(Vector4(-4.0, 0.0, 0.0), Vector4(0.0, 1.0, 0.0), Vector4(1.0, 0.0, 0.0), 60.0);
    auto image = Image(1280, 1920);
    auto scene = Scene(1280, 1920, camera);
    auto material = Material(Color(0, 255, 0),1.0, 1.0, 1.0);
    auto material2 = Material(Color(0, 0, 255), 1.0, 0.5, 1.0);
    auto materiel3 = Material(Color(42, 42, 42), 0, 0.4, 1.0);
    Sphere sphere = Sphere(Vector4(22.0, 0.0, 2.5), material, 2.0);
    Sphere sphere2 = Sphere(Vector4(18.0, 1.0, -2.0), material2, 2.5);
    Plane plane = Plane(Vector4(0, 4, 0), materiel3, Vector4(1, 0, 0), Vector4(0, 0, 1));
    Plane plane1 = Plane(Vector4(0, 0, 6), materiel3, Vector4(1, 0, 0), Vector4(0, 1, 0));
    Plane plane2 = Plane(Vector4(0, 0, -6), materiel3, Vector4(1, 0, 0), Vector4(0, 1, 0));
    Plane plane3 = Plane(Vector4(26, 0, 0), materiel3, Vector4(0, 0, 1), Vector4(0, 1,0 ));

    auto light = Light(Vector4(18, -4.0, 0), Color(255, 0, 0));

    scene.add_object(std::make_shared<Sphere>(sphere2));
    scene.add_object(std::make_shared<Sphere>(sphere));
    scene.add_object(std::make_shared<Plane>(plane));
    scene.add_object(std::make_shared<Plane>(plane1));
    scene.add_object(std::make_shared<Plane>(plane2));


    scene.add_light(light);*/

    Scene scene(argv[1]);
    auto camera = scene.get_camera();
    auto image = Image(scene.get_height(), scene.get_width());

    for (int y = 0; y < scene.get_height(); y++)
    {
        for (int x = 0; x < scene.get_width(); x++)
        {
            auto pos = scene.get_pixel_pos(x, y);
            auto dir = (pos - camera.get_center()).normalize();

            auto color = scene.cast_ray(camera.get_center(), dir);
            image.set_pixel(x, y, color);
        }
    }
    image.compute_image();

    return 0;
}
