//
// Created by gigi on 2/13/20.
//

#include "color.hh"

float Color::get_red() const
{
    return this->_red;
}

float Color::get_green() const
{
    return this->_green;
}

float Color::get_blue() const
{
    return this->_blue;
}


std::ostream& operator<<(std::ostream &out, Color &color)
{
    return out << (int)color._red << " " << (int)color._green << " " << (int)color._blue << " ";
}

Color Color::operator*(const Color &rhs)
{
    Color n_col =  Color(_red * rhs._red, _green * rhs._green, _blue * rhs._blue);
    n_col.check_bound();
    return n_col;
}

Color Color::operator*(const float coef)
{
    Color n_col =  Color(_red * coef, _green * coef, _blue * coef);
    n_col.check_bound();
    return n_col;
}

Color Color::operator+(const Color &rhs)
{
    Color n_col =  Color(_red + rhs._red, _green + rhs._green, _blue + rhs._blue);
    n_col.check_bound();
    return n_col;
}

Color Color::operator+=(const Color &rhs)
{
    return *this + rhs;
}

Color Color::operator-(const Color &rhs)
{
    Color n_col =  Color(_red - rhs._red, _green - rhs._green, _blue - rhs._blue);
    n_col.check_bound();
    return n_col;
}

void Color::check_bound()
{
    if (_red > 255)
        _red = 255;
    if (_green > 255)
        _green = 255;
    if (_blue > 255)
        _blue = 255;
    if (_red < 0)
        _red = 0;
    if (_green < 0)
        _green = 0;
    if (_blue < 0)
        _blue = 0;
}
