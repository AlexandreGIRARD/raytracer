//
// Created by gigi on 2/14/20.
//
#include <cmath>
#include <fstream>

#include "scene.hh"
#include "json.hpp"

Scene::Scene(int height, int width, const Camera &camera)
    : _height(height)
    , _width(width)
    , _camera(camera)
{
    this->_objects = std::vector<std::shared_ptr<Object>>();
    this->_lights = std::vector<Light>();

    this->_fov = tan((camera.get_fov() * (3.141592654f / 180)) * 0.5);

    if (this->_width > this->_height)
        this->_ratio_x = double(width) / height;
    else
        this->_ratio_y = double(height) / width;
}


Scene::Scene(char *json_file)
{
    std::ifstream file(json_file);
    nlohmann::json j;
    file >> j;

    // Global info
    _height = j["height"];
    _width = j["width"];
    _camera = Camera(Vector4(j["camera"]["pos"]), Vector4(j["camera"]["up"]), Vector4(j["camera"]["forward"]), j["camera"]["fov"]);
    _fov = tan((_camera.get_fov() * (3.141592654f / 180)) * 0.5);
    if (_width > _height)
        _ratio_x = double(_width) / _height;
    else
        _ratio_y = double(_height) / _width;

    // Material
    for (auto mtl : j["materials"])
        _materials[mtl["name"]] = Material(mtl);

    // Objects
    _objects = std::vector<std::shared_ptr<Object>>();
    for (auto obj : j["objects"])
    {
        if (obj["type"] == "sphere")
            add_object(std::make_shared<Sphere>(obj, _materials.find(obj["material"])->second));
        else if (obj["type"] == "plane")
            add_object(std::make_shared<Plane>(obj, _materials.find(obj["material"])->second));
        else if (obj["type"] == "triangle")
            add_object(std::make_shared<Triangle>(obj, _materials.find(obj["material"])->second));
    }

    // Lights
    for (auto light : j["lights"])
        _lights.emplace_back(Light(light));
}

void Scene::add_light(Light &light)
{
    this->_lights.emplace_back(light);
}

void Scene::add_object(std::shared_ptr<Object> object)
{
    this->_objects.emplace_back(object);
}

std::vector<Light> Scene::get_lights() const
{
    return _lights;
}

std::vector<std::shared_ptr<Object>> Scene::get_objects() const
{
    return _objects;
}

int Scene::get_height()
{
    return _height;
}

int Scene::get_width()
{
    return _width;
}

Camera Scene::get_camera() const
{
    return _camera;
}

Vector4 Scene::get_pixel_pos(int x, int y)
{
    double offset_x = _fov * (((double)x / _width) - 0.5) * _ratio_x;
    double offset_y = _fov * (((double)y / _height) - 0.5) * _ratio_y;
    Vector4 middle = _camera.get_center() + _camera.get_forward();
    return middle + _camera.get_up() * offset_y + _camera.get_right() * offset_x;
}

Color Scene::get_color(const Object &obj, const Vector4 &point, const Vector4 &vect, int iter)
{
    auto mtl = obj.get_material();
    Vector4 normal = obj.get_normal(point);
    Vector4 s = (vect - normal * 2.0 * normal.dot(vect)).normalize();
    auto epsi_point = point + s * 0.1;

    Color total_diffuse;
    Color total_specular;

    for (auto light : _lights) {
        Vector4 l = (light.get_pos() - point).normalize();
        // Shadow check
        auto dist_light = std::sqrt(l.dot(l));
        for (auto obj : _objects) {
            auto inter = obj->intersection(epsi_point, l);
            if (inter.has_value() && inter.value().distance(epsi_point) < dist_light)
                continue;
        }
        total_diffuse = mtl.get_color() - (mtl.get_color() * normal.dot(l) * light.get_intensity() * mtl.get_diffuse()) + total_diffuse;
        total_specular = total_specular + light.get_intensity() * pow(s.dot(l), mtl.get_reflectivity()) * 255.0 * mtl.get_specular() + total_specular;
    }
    if (iter)
        return total_diffuse + total_specular + cast_ray(epsi_point, s, iter - 1) * mtl.get_specular();
    return total_diffuse + total_specular;
}

Color Scene::cast_ray(const Vector4 &origin, const Vector4 &dir, int iter)
{
    auto dist = INFINITY;
    Vector4 intersection;
    int id = -1;
    for (int i = 0; i < _objects.size(); i++)
    {
        auto inter = _objects[i]->intersection(origin, dir);
        if (!inter.has_value())
            continue;
        Vector4 val = inter.value();
        auto tmp = val.distance(origin);
        if (dist > tmp && tmp > 0)
        {
            intersection = val;
            id = i;
            dist = tmp;
        }
    }
    if (dist == INFINITY)
        return Color(0,0,0);

    return get_color(*_objects[id], intersection, dir, iter);
}
