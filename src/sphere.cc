//
// Created by gigi on 2/14/20.
//

#include "sphere.hh"
#include <cmath>
#include <algorithm>

std::optional<Vector4> Sphere::intersection(const Vector4 &origin, const Vector4 &vect)
{
    auto radius2 = this->_r * this->_r;
    float t0, t1;
    Vector4 l = this->_position - origin;
    float tca = l.dot(vect);
    if (tca < 0)
        return std::nullopt;
    float d2 = l.dot(l) - (tca * tca);

    if (d2 > radius2)
        return std::nullopt;
    auto thc = sqrt(radius2 - d2);

    t0 = tca - thc;
    t1 = tca + thc;

    if (t0 > t1)
        std::swap(t0, t1);
    if (t0 < 0)
    {
        t0 = t1;
        if (t0 < 0)
            return std::nullopt;
    }

    return std::make_optional(origin + vect * t0);
}

Vector4 Sphere::get_normal(const Vector4 &point) const
{
    return (_position - point).normalize();
}