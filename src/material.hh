//
// Created by gigi on 2/21/20.
//

#ifndef RAYTRACER_MATERIAL_HH
#define RAYTRACER_MATERIAL_HH

#include "color.hh"
#include "json.hpp"

class Material {
public:
    Material()
    {}
    Material(const Color &color, const float &diffuse, const float &specular, const float reflectivity)
        : _color(color)
        , _diffuse(diffuse)
        , _specular(specular)
        , _reflectivity(reflectivity)
    {}

    Material(nlohmann::json json)
        : _color(Color(json["color"]))
        , _diffuse(json["diffuse"])
        , _specular(json["specular"])
        , _reflectivity(json["reflectivity"])
    {}

    Color get_color() const
    {
        return _color;
    }

    float get_diffuse() const
    {
        return _diffuse;
    }

    float get_specular() const
    {
        return _specular;
    }

    float get_reflectivity() const
    {
        return _reflectivity;
    }

private:
    Color _color;
    float _diffuse;
    float _specular;
    float _reflectivity;
};


#endif //RAYTRACER_MATERIAL_HH
