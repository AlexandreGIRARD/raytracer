//
// Created by gigi on 2/13/20.
//

#ifndef RAYTRACER_VECTOR4_H
#define RAYTRACER_VECTOR4_H

#include <iostream>

#include "json.hpp"


class Vector4 {
public:
    Vector4()
        : _x(0)
        , _y(0)
        , _z(0)
        , _r(0)
    {}

    Vector4(double x, double y, double z)
        : _x(x)
        , _y(y)
        , _z(z)
        , _r(0)
    {}

    Vector4(nlohmann::json j)
        : _x(j[0])
        , _y(j[1])
        , _z(j[2])
        , _r(0)
    {}

    Vector4 operator+(const Vector4 &vect) const;
    Vector4 operator-(const Vector4 &vect) const;
    Vector4 operator*(const double lambda) const;
    Vector4 operator/(const double lambda)const ;

    Vector4 cross(const Vector4 &vect) const;
    float dot(const Vector4 &vect) const;
    float norm();
    float distance(const Vector4 &rhs);
    Vector4 normalize();

    friend std::ostream&operator<<(std::ostream &out, Vector4 &vector4);

private:
    double _x;
    double _y;
    double _z;
    double _r;
};


#endif //RAYTRACER_VECTOR4_H
