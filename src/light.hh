//
// Created by gigi on 2/14/20.
//

#ifndef RAYTRACER_LIGHT_HH
#define RAYTRACER_LIGHT_HH

#include "vector4.hh"
#include "color.hh"

class Light {
public:
    Light(const Vector4 &pos, const Color &color)
        : _pos(pos)
    {
        _intensity = Color(color.get_red() / 255.0, color.get_green() / 255.0, color.get_blue() / 255.0);
    }

    Light(nlohmann::json j)
        : _pos(Vector4(j["position"]))
    {
        _intensity = Color((float)j["color"][0] / 255.0, (float)j["color"][1] / 255.0, (float)j["color"][2] / 255.0);
    }

    Vector4 get_pos()
    {
        return _pos;
    }

    Color get_intensity()
    {
        return _intensity;
    }

private:
    Vector4 _pos;
    Color _intensity;
};

#endif //RAYTRACER_LIGHT_HH
