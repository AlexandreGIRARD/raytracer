//
// Created by gigi on 2/29/20.
//

#include "plane.hh"

std::optional<Vector4> Plane::intersection(const Vector4 &point, const Vector4 &vect)
{
    if (vect.dot(_normal) == 0)
        return std::nullopt;
    auto dist = (_position - point).dot(_normal) / vect.dot(_normal);
    if (dist < 0)
        return std::nullopt;
    return point + vect * dist;
}

Vector4 Plane::get_normal(const Vector4 &point) const
{
    return _normal;
}