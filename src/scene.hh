//
// Created by gigi on 2/14/20.
//

#ifndef RAYTRACER_SCENE_H
#define RAYTRACER_SCENE_H

#include "sphere.hh"
#include "plane.hh"
#include "triangle.hh"
#include "camera.hh"
#include "light.hh"

#include <vector>
#include <memory>
#include <map>


class Scene {
public:
    Scene(int height, int width, const Camera &camera);
    Scene(char *json_file);

    void add_object(std::shared_ptr<Object> object);
    void add_light(Light &light);

    Vector4 get_pixel_pos(int x, int y);
    int get_height();
    int get_width();

    std::vector<std::shared_ptr<Object>> get_objects() const;
    std::vector<Light> get_lights() const;
    Camera get_camera() const;

    Color cast_ray(const Vector4 &origin, const Vector4 &dir, int iter = 5);
    Color get_color(const Object &obj, const Vector4 &point, const Vector4 &vect, int iter);

private:
    int _height;
    int _width;
    float _ratio_x = 1;
    float _ratio_y = 1;
    float _fov;

    Camera _camera;

    std::vector<std::shared_ptr<Object>> _objects;
    std::vector<Light> _lights;
    std::map<std::string, Material> _materials;
};


#endif //RAYTRACER_SCENE_HH
