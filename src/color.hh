//
// Created by gigi on 2/13/20.
//

#ifndef RAYTRACER_COLOR_H
#define RAYTRACER_COLOR_H

#include <cstdint>
#include <iostream>

#include "json.hpp"

class Color {
public:
    Color()
        : _red(0)
        , _green(0)
        , _blue(0)
    {}

    Color(const float red, const float green, const float blue)
        : _red(red),
        _green(green),
        _blue(blue)
    {}

    Color(const Color &color)
        : _red(color._red),
        _green(color._green),
        _blue(color._blue)
    {}

    Color(nlohmann::json json)
        : _red(json[0])
        , _green(json[1])
        , _blue(json[2])
    {}

    float get_red() const;
    float get_green() const;
    float get_blue() const;

    Color operator*(const Color &rhs);
    Color operator*(const float coef);
    Color operator+(const Color &rhs);
    Color operator-(const Color &rhs);
    Color operator+=(const Color &rhs);


    friend std::ostream& operator<<(std::ostream &out, Color &color);

    void check_bound();

private:

    float _red;
    float _green;
    float _blue;
};


#endif //RAYTRACER_COLOR_H
