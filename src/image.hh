//
// Created by gigi on 2/13/20.
//

#ifndef RAYTRACER_IMAGE_H
#define RAYTRACER_IMAGE_H

#include <vector>
#include "color.hh"

class Image {
public:
    Image(const int height, const int width)
        : _width(width),
        _height(height)
    {
        this->_pixels = std::vector<Color>(height * width, Color());
    }

    void compute_image();
    void set_pixel(const int i,const int j, const Color &color);

private:
    int _width;
    int _height;
    std::vector<Color> _pixels;
};


#endif //RAYTRACER_IMAGE_H
