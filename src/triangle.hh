//
// Created by gigi on 3/1/20.
//

#ifndef RAYTRACER_TRIANGLE_HH
#define RAYTRACER_TRIANGLE_HH

#include "object.hh"

class Triangle : public Object{
public:
    Triangle(Vector4 &position, Material &material, Vector4 &b, Vector4 &c, Vector4 &normal)
        : Object(position, material)
        , _b(b)
        , _c(c)
        , _normal(normal)
    {}

    Triangle(nlohmann::json json,Material &material)
        : Object(json["position"], material)
        , _b(Vector4(json["b"]))
        , _c(Vector4(json["c"]))
    {
        auto ab = (_b - _position).normalize();
        auto ac = (_c - _position).normalize();
        _normal = ab.cross(ac);
    }

    virtual std::optional<Vector4> intersection(const Vector4 &point,const Vector4 &vect) override;
    virtual Vector4 get_normal(const Vector4 &point) const override;

private:
    Vector4 _b;
    Vector4 _c;
    Vector4 _normal;
};


#endif //RAYTRACER_TRIANGLE_HH
